from django.contrib.auth.models import User
from rest_framework.relations import SlugRelatedField, PrimaryKeyRelatedField
from rest_framework.serializers import ModelSerializer

from blog.models import Post, Category


class CategorySerializer(ModelSerializer):
    def to_representation(self, instance):
        return instance.name

    class Meta:
        model = Category
        fields = 'name',


class AllDataPostSerializer(ModelSerializer):
    author = SlugRelatedField(
        queryset=User.objects.all(), slug_field='username'
    )
    category = CategorySerializer(read_only=True, many=True)

    class Meta:
        model = Post
        fields = ('id', 'title', 'text', 'author', 'category', 'created_date', 'changed_date')


class SomeDataSerializer(ModelSerializer):
    category = PrimaryKeyRelatedField(allow_empty=False, many=True, queryset=Category.objects.all())
    author = SlugRelatedField(
        queryset=User.objects.all(), slug_field='username'
    )

    class Meta:
        model = Post
        exclude = ('id', 'created_date', 'changed_date')


class SmallDataPostSerializer(ModelSerializer):
    class Meta:
        model = Post
        fields = ('title', 'text')
