from rest_framework.decorators import api_view
from rest_framework.generics import get_object_or_404
from rest_framework.response import Response

from blog.models import Post
from blog.serializers import AllDataPostSerializer, SmallDataPostSerializer, SomeDataSerializer


@api_view()
def get_posts(request):
    posts = Post.objects.all()
    resp = AllDataPostSerializer(posts, many=True)
    return Response({'posts': resp.data})


@api_view(['GET', 'POST'])
def get_add_posts(request):
    if request.method == 'POST':
        post = request.data.get('post')
        post['author'] = request.user.username
        ser_model = SomeDataSerializer(data=post)
        if ser_model.is_valid(raise_exception=True):
            saved_model = ser_model.save()
        return Response({'status': 'ok', 'message': f'{saved_model.title} successfully created.'})
    posts = Post.objects.all()
    resp = SomeDataSerializer(posts, many=True)
    return Response({'posts': resp.data})


@api_view(['GET', 'PUT'])
def edit_post(request, pk=None):
    post = get_object_or_404(Post.objects.all(), pk=pk)
    resp = SmallDataPostSerializer(post)
    if request.method == 'PUT':
        post_to_update = get_object_or_404(Post.objects.all(), pk=pk)
        post_data = request.data.get('post')
        ser_model = SmallDataPostSerializer(instance=post_to_update, data=post_data)
        if ser_model.is_valid(raise_exception=True):
            saved_model = ser_model.save()
        return Response({'status': 'ok', 'message': f'{saved_model.title} successfully updated.'})
    return Response({'post': resp.data})

